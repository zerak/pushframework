
// -*- C++ -*-
//==========================================================
/**
 *     Created_datetime : 10/25/2013 14:32
 *     File_base        : main
 *     File_ext         : h
 *     Author           : GNUnix <Kingbug2010@gmail.com>
 *     Description      : 
 *
 *     <Change_list>
 */
//==========================================================
#ifndef _main_hpp_
#define _main_hpp_

#include <iostream>
using namespace std;

#include "Plateform.h"
#include "PushFrameworkInc.h"
using namespace PushFramework;

//
#include "ClientFactoryImp.h"

int main()
{
	Server *srv = new Server();

	// set client factory
	ClientFactoryImp factory;
	srv->setClientFactory(&factory);

	// set listener opetions
	ListenerOptions lisOps;
	lisOps.interfaceAddress = "192.168.1.74";
	lisOps.isTcpNoDelay = true;
	lisOps.listeningBackLog = 5;
	srv->createListener( 60000, &lisOps );

	PushFramework::options.nMaxConnections = 10000;
	options.nMaxConnections = 10000;
	options.uLoginExpireDuration = 60;
	options.nWorkersCount = 1;
	//options.nStreamers = 0;
	//options.isProfilingEnabled = true;
	//options.samplingRate = 10;
	//options.isMonitorEnabled = true;
	//options.monitorPort = 60001;
	//options.password = "123";
	options.uMaxClientIdleTime = 120;
	options.usePools = true;
	options.nPhysicalConnectionsPoolSize = 10000;
	options.uReadBufferSize = 2000;
	options.uSendBufferSize = 2000;
	options.uIntermediateSendBufferSize = 2000;
	options.uIntermediateReceiveBufferSize = 2000;

	srv->start();

	system("pause");
	return 0;
}

#endif