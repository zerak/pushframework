// -*- C++ -*-
//==========================================================
/**
 *     Created_datetime : 10/25/2013 14:47
 *     File_base        : ClientFactoryImp
 *     File_ext         : h
 *     Author           : GNUnix <Kingbug2010@gmail.com>
 *     Description      : 
 *
 *     <Change_list>
 */
//==========================================================
#ifndef _ClientFactoryImp_hpp_
#define _ClientFactoryImp_hpp_

//#include "IncomingPacket.h"
//#include "ConnectionContext.h"
//#include "LogicalConnection.h"
//#include "OutgoingPacket.h"
#include "ClientFactory.h"
using namespace PushFramework;

class ClientFactoryImp : public ClientFactory
{
public:
	ClientFactoryImp(void);
	~ClientFactoryImp(void);

public:
	virtual int onFirstRequest(PushFramework::IncomingPacket& request, ConnectionContext* pConnectionContext, LogicalConnection*& lpClient, OutgoingPacket*& lpPacket);
};


#endif
